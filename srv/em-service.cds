using { sap.capire.energymanager as my } from '../db/schema';
service EnergyManagerService @(path:'/browse') {

  @readonly entity Books as SELECT from my.Books {*,
    author.name as author
  } excluding { createdBy, modifiedBy };

  @requires_: 'authenticated-user'
  @insertonly entity Orders as projection on my.Orders;

  entity Buildings as projection on my.Buildings;
  entity Floors as projection on my.Floors;
  entity Rooms as projection on my.Rooms;
  entity Sensors as projection on my.Sensors;
  entity Equipments as projection on my.Equipments;
  entity Sensor_Data as projection on my.Sensor_Data;
  entity Sensor_Data_Equipments_Links as projection on my.Sensor_Data_Equipments_Links;

}

annotate EnergyManagerService.Buildings with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Building',
        TypeNamePlural: 'Buildings',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, name, descr, average_daily_consumption],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: name},
          {$Type: 'UI.DataField', Value: descr},
          {$Type: 'UI.DataField', Value: average_daily_consumption}
      ]
    }
);

annotate EnergyManagerService.Buildings with {
  ID @( Common: { Label: 'Building ID'} );
  name @( Common.Label: 'Name' );
  descr @( Common.Label: 'Building Descr' );
  average_daily_consumption @( Common.Label: 'AVG');
}

annotate EnergyManagerService.Floors with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Floor',
        TypeNamePlural: 'Floors',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, name, descr, average_daily_consumption],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: name},
          {$Type: 'UI.DataField', Value: descr},
          {$Type: 'UI.DataField', Value: average_daily_consumption}
      ]
    }
);

annotate EnergyManagerService.Floors with {
  ID @( Common: { Label: 'Floor ID'} );
  name @( Common.Label: 'Name' );
  descr @( Common.Label: 'Floor Descr' );
  average_daily_consumption @( Common.Label: 'AVG');
}

annotate EnergyManagerService.Rooms with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Room',
        TypeNamePlural: 'Rooms',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, name, descr, average_daily_consumption],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: name},
          {$Type: 'UI.DataField', Value: descr},
          {$Type: 'UI.DataField', Value: average_daily_consumption}
      ]
    }
);

annotate EnergyManagerService.Rooms with {
  ID @( Common: { Label: 'Room ID'} );
  name @( Common.Label: 'Name' );
  descr @( Common.Label: 'Room Descr' );
  average_daily_consumption @( Common.Label: 'AVG');
}

annotate EnergyManagerService.Sensors with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Sensor',
        TypeNamePlural: 'Sensors',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, name, descr],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: name},
          {$Type: 'UI.DataField', Value: descr}
      ]
    }
);

annotate EnergyManagerService.Sensors with {
  ID @( Common: { Label: 'Sensor ID'} );
  name @( Common.Label: 'Name' );
  descr @( Common.Label: 'Room Descr' );
}

annotate EnergyManagerService.Sensor_Data with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Sensor',
        TypeNamePlural: 'Sensors',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, current, voltage,power,cosphi,frequency,energypos,energyneg,timestamp],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: current},
          {$Type: 'UI.DataField', Value: voltage},
          {$Type: 'UI.DataField', Value: power},
          {$Type: 'UI.DataField', Value: cosphi},
          {$Type: 'UI.DataField', Value: frequency},
          {$Type: 'UI.DataField', Value: energypos},
          {$Type: 'UI.DataField', Value: energyneg},
          {$Type: 'UI.DataField', Value: timestamp}
      ]
    }
);

annotate EnergyManagerService.Sensor_Data with {
  ID @( Common: { Label: 'Sensor_Data ID'} );
  current @( Common.Label: 'Current' );
  voltage @( Common.Label: 'Voltage' );
  power @( Common.Label: 'Power' );
  cosphi @( Common.Label: 'Cosphi' );
  frequency @( Common.Label: 'Frequency' );
  energypos @( Common.Label: 'Energypos' );
  energyneg @( Common.Label: 'Energyneg' );
  timestamp @( Common.Label: 'Timestamp' );
}

annotate EnergyManagerService.Equipments with @(
    UI: {
      HeaderInfo: {
        TypeName: 'Equipment',
        TypeNamePlural: 'Equipments',
        Title: { $Type: 'UI.DataField', Value: title }
      },
      SelectionFields: [ID, line_no, name,type,descr,in_use,average_daily_consumption],
      LineItem: [
          {$Type: 'UI.DataField', Value: ID},
          {$Type: 'UI.DataField', Value: line_no},
          {$Type: 'UI.DataField', Value: name},
          {$Type: 'UI.DataField', Value: type},
          {$Type: 'UI.DataField', Value: descr},
          {$Type: 'UI.DataField', Value: in_use},
          {$Type: 'UI.DataField', Value: average_daily_consumption}
      ]
    }
);

annotate EnergyManagerService.Equipments with {
  ID @( Common: { Label: 'Sensor ID'} );
  line_no @( Common.Label: 'Line_no' );
  name @( Common.Label: 'Name' );
  type @( Common.Label: 'Type' );
  descr @( Common.Label: 'Descr' );
  in_use @( Common.Label: 'In_use' );
  average_daily_consumption @( Common.Label: 'AVG' );
}