namespace sap.capire.energymanager;
using { Currency, managed } from '@sap/cds/common';

entity Buildings : managed {
  key ID   : Integer;
  name    : localized String(111);
  descr    : localized String(1111);
  average_daily_consumption: Double;
}

entity Floors : managed {
  key ID   : Integer;
  building : Association to Buildings;
  name    : localized String(111);
  descr    : localized String(1111);
  average_daily_consumption: Double;
}

entity Rooms : managed {
  key ID   : Integer;
  floor : Association to Floors;
  name    : localized String(111);
  descr    : localized String(1111);
  average_daily_consumption: Double;
}

entity Equipments : managed {
  key ID   : Integer;
  building : Association to Buildings;
  floor : Association to Floors;
  room : Association to Rooms;
  sensor : Association to Sensors;
  line_no : Integer;
  name : localized String(111);
  type : localized String(111);
  descr    : localized String(1111);
  in_use : Boolean;
  average_daily_consumption: Double;
}

entity Sensors : managed {
  key ID  : Integer;
  name    : localized String(111);
  descr    : localized String(1111);
}

entity Sensor_Data : managed {
  key ID  : Integer64;
  sensor : Association to Sensors;
  current : Double;
  voltage : Double;
  power : Double;
  cosphi : Double;
  frequency : Double;
  energypos : Double;
  energyneg : Double;
  timestamp : Timestamp;
}

entity Sensor_Data_Equipments_Links : managed {
  key ID_Data : Association to Sensor_Data;
  key ID_Equipment : Association to Equipments;
}

entity Books : managed {
  key ID   : Integer;
  title    : localized String(111);
  descr    : localized String(1111);
  author   : Association to Authors;
  stock    : Integer;
  price    : Decimal(9,2);
  currency : Currency;
}

entity Authors : managed {
  key ID   : Integer;
  name     : String(111);
  books    : Association to many Books on books.author = $self;
}

entity Orders : managed {
  key ID   : UUID;
  OrderNo  : String @title:'Order Number'; //> readable key
  Items    : Composition of many OrderItems on Items.parent = $self;
}
entity OrderItems {
  key ID   : UUID;
  parent   : Association to Orders;
  book     : Association to Books;
  amount   : Integer;
}